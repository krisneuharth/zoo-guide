DATA = {
    'animals': [
        {
            "id": 1,
            "name": "Elephant, Asian",
            "scientific_name": "Elephas maximus indicus",
            "url": "http://www.lazoo.org/animals/mammals/asian-elephant/",
            "uri": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Stuff&w=120&h=120",
            "snippet": "The elephant is the world’s largest living land mammal. Known for their intelligence and social behavior, these pachyderms can communicate with each other using infrasound over a distance of more than two miles."
        },
        {
            "id": 2,
            "name": "Gorilla, Western Lowland",
            "scientific_name": "Gorilla gorilla gorilla",
            "url": "http://www.lazoo.org/animals/mammals/western-lowland-gorilla/",
            "uri": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Stuff&w=120&h=120",
            "snippet": "Gorillas are the strongest and largest of the great apes, a primate group that also includes chimpanzees, bonobos, and orangutans. The western lowland gorilla is a subspecies of western gorilla (Gorilla gorilla). All gorillas in U.S. zoos are western lowland gorillas."
        },
        {
            "id": 3,
            "name": "Lion, African",
            "scientific_name": "Panthera leo",
            "url": "http://www.lazoo.org/animals/mammals/african-lion/",
            "uri": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Stuff&w=120&h=120",
            "snippet": "Lions are perfect hunting machines. They have great binocular vision to judge distances. In bright light, the pupils constrict to round points so the lion can hunt during the day without being blinded by the sun. At night, their hunting ability is enhanced because the pupils of their eyes let in more light by dilating to three times the size of a human’s pupils. Keen hearing and sense of smell enable lions to detect hidden prey with ease."
        }
    ]
};

module.exports = DATA;