'use strict';

import React, {
    AppRegistry,
    Component,
    StatusBarIOS
} from 'react-native';

import RootRouter from './src/components/RootRouter';
StatusBarIOS.setStyle('light-content');

class App extends Component {
    render() {
        return (
            <RootRouter />
        );
    }
}

AppRegistry.registerComponent('ZooGuide', () => App);
