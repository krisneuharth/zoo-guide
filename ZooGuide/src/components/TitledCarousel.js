'use strict';

import React from 'react-native';
var {
    Text,
    View,
    Component
    } = React;

import Carousel from "react-native-carousel-control";

var _ = require('underscore');


import Heart from '../components/Heart';
import Card from '../components/Card';
import Separator from '../components/Separator';

import {styles} from '../styles/Styles';

var ANIMALS = require('../../data/Animals.js');



export default class TitledCarousel extends Component {
    render() {
        return (
            <View>
                <View style={styles.paddedContent}>
                    <Text style={styles.header}>{this.props.text}</Text>
                    <Separator/>
                </View>

                <Carousel
                    pageStyle={{
                        marginLeft: 0,
                        marginBottom: 10,
                        backgroundColor: "white",
                        borderColor: "green",
                        borderWidth: 1,
                        borderRadius: 4,
                        width: 130,
                        height: 130
                    }}
                    sneak={3}>

                    {
                        this.props.animals.map(
                            function (id, ii) {
                                var animal = _.findWhere(ANIMALS.animals, {id: id});

                                return (
                                    <Card id={animal.id}
                                          text={animal.name}
                                          uri={animal.uri}
                                    />
                                )
                            }
                        )
                    }
                </Carousel>
            </View>
        )
    }
}


