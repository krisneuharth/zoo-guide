'use strict';

import React from 'react-native';
var {
    Component
} = React;

var Icon = require('react-native-vector-icons/FontAwesome');

import {
    styles,
    HEART_COLOR,
    HEART_COLOR_SELECTED,
    HEART_SIZE,
} from '../styles/Styles';


export default class Heart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: this.props.selected
        }
    }

    onPress() {
        this.setState({
            selected: !this.state.selected
        })
    }

    render() {
        var color = this.state.selected ? HEART_COLOR_SELECTED : HEART_COLOR;

        return (
            <Icon
                name="heart"
                size={HEART_SIZE}
                color={color}
                style={styles.heart}
                onPress={this.onPress.bind(this)}
            />
        )
    }
}
