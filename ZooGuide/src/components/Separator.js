'use strict';

import React from 'react-native';
var {
    View,
    Component
} = React;

import {styles} from '../styles/Styles';

export default class Separator extends Component {
    render() {
        return (
            <View style={styles.lineSeparator}/>
        )
    }
}
