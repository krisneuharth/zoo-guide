'use strict';

import React from 'react-native';
var {
    Component
} = React;

import {Actions} from 'react-native-router-flux';
import AppEventEmitter from '../services/AppEventEmitter';
var Icon = require('react-native-vector-icons/FontAwesome');

import {
    styles,
    NAV_TITLE_COLOR,
    NAV_HAMBURGER_SIZE,
} from '../styles/Styles';


export default class Hamburger extends Component {
    onPress() {
        AppEventEmitter.emit('hamburger.press');
    }

    render() {
        return (
            <Icon
                name="bars"
                size={NAV_HAMBURGER_SIZE}
                color={NAV_TITLE_COLOR}
                style={styles.hamburger}
                onPress={this.onPress.bind(this)}
            />
        )
    }
}
