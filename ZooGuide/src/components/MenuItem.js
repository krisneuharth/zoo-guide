'use strict';

import React, {
    Component,
    Text,
    View,
    ScrollView,
    Image,
    TouchableHighlight
} from 'react-native';

var Icon = require('react-native-vector-icons/FontAwesome');

import AppEventEmitter from '../services/AppEventEmitter';


import {
    styles,
    DARK_PRIMARY_COLOR,
    TEXT_PRIMARY_COLOR,
    LIGHT_PRIMARY_COLOR,
    ACCENT_COLOR,
    MENU_ICON_SIZE
} from '../styles/Styles';


export default class MenuItem extends Component {
    clickMenuItem() {
        AppEventEmitter.emit('menuItem.press', { selected: this.props.name });
        this.props.gotoView()
    }

    render() {
        return (
            <TouchableHighlight
                onPress={(() => this.clickMenuItem())}
                style={styles.menuItemContainer}
                underlayColor={ACCENT_COLOR}>
                <View style={this.props.selected ? styles.menuItemSelected : styles.menuItem}>
                    <Icon name={this.props.iconName} size={MENU_ICON_SIZE} color={TEXT_PRIMARY_COLOR}
                          style={styles.menuItemIcon}/>

                    <Text style={styles.menuItemText}>{this.props.name}</Text>
                </View>
            </TouchableHighlight>
        )
    }
}