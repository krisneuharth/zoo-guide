'use strict';

import React from 'react-native';
var {
    Component
} = React;

import {Actions} from 'react-native-router-flux';
import AppEventEmitter from '../services/AppEventEmitter';

import {
    styles,
    NAV_TITLE_COLOR,
    DARK_PRIMARY_COLOR
} from '../styles/Styles';

var NavigationBar = require('react-native-navbar');

import Hamburger from './Hamburger';
import Back from './Back';


export default class NavBar extends Component {
    render() {
        return (
            <NavigationBar
                style={styles.navigationBar}
                tintColor={DARK_PRIMARY_COLOR}
                title={{title: this.props.title, tintColor: NAV_TITLE_COLOR}}
                leftButton={
                    this.props.backEnabled ? <Back/> : <Hamburger/>
                }
            />
        )
    }
}
