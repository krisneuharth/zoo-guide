'use strict';

import React, {Component, Navigator, Text, View} from 'react-native';
import {Router, Route, Schema, Actions} from 'react-native-router-flux';
import EventEmitter from 'EventEmitter';
import Drawer from 'react-native-drawer'
import AppEventEmitter from '../services/AppEventEmitter';

var _ = require('underscore');

import Home from '../../src/views/Home';
import About from '../../src/views/About';
import Animal from '../../src/views/Animal';
import Animals from '../../src/views/Animals';
import Event from '../../src/views/Event';
import Events from '../../src/views/Events';
import Favorites from '../../src/views/Favorites';
import Map from '../../src/views/Map';
import Menu from '../../src/views/Menu';
import Search from '../../src/views/Search';
import Settings from '../../src/views/Settings';

import {styles} from '../styles/Styles';

var ANIMALS = require('../../data/Animals.js');
var EVENTS = require('../../data/Events.js');


export default class RootRouter extends Component {

	componentDidMount() {
		AppEventEmitter.addListener('hamburger.press', this.toggleMenu.bind(this));
		AppEventEmitter.addListener('back.press', this.back.bind(this));
		AppEventEmitter.addListener('card.press', this.card.bind(this));
    }

    componentWillUnMount() {
    	AppEventEmitter.removeListener('hamburger.press');
    	AppEventEmitter.removeListener('back.press');
    	AppEventEmitter.removeListener('card.press');
    }

	closeMenu(navigation) {
		if(navigation.type == 'AFTER_ROUTER_ROUTE') {
			this.refs.drawer.close();
		}
	}

	toggleMenu() {
	   this.refs.drawer.toggle();
	}

	back() {
		Actions.pop();
	}

	card(args) {
		var animal = _.findWhere(ANIMALS.animals, {id: args.id});

		Actions.animal({
            animal: animal
        });
	}

    render() {
        return(
        	<Drawer
        		ref="drawer"
				type="static"
  				tapToClose={true}
				openDrawerOffset={100}
				initializeOpen={false}
          		styles={{main: {shadowColor: "#000000", shadowOpacity: 0.4, shadowRadius: 3}}}
          		tweenHandler={Drawer.tweenPresets.parallax}
				content={<Menu/>}
				>
					<View style={styles.container}>

			            <Router hideNavBar={true} dispatch={this.closeMenu.bind(this)}>

			                <Schema name="default" sceneConfig={Navigator.SceneConfigs.FloatFromRight}/>

                            <Route name="home" 		component={Home} initial={true}/>

			                <Route name="about" 	wrapRouter={false} component={About} 	 title="About" />
			                <Route name="animal" 	wrapRouter={false} component={Animal} 	 title="Animal" />
			                <Route name="animals" 	wrapRouter={false} component={Animals} 	 title="Animals" />
			                <Route name="event" 	wrapRouter={false} component={Event} 	 title="Event" />
			                <Route name="events" 	wrapRouter={false} component={Events} 	 title="Events" />
			                <Route name="favorites" wrapRouter={false} component={Favorites} title="Favorites" />
			                <Route name="map" 		wrapRouter={false} component={Map} 		 title="Map" />
			                <Route name="search" 	wrapRouter={false} component={Search} 	 title="Search" />
			                <Route name="settings" 	wrapRouter={false} component={Settings}  title="Settings" />
			            </Router>
		            </View>
            </Drawer>
        );
    }
}