'use strict';

import React from 'react-native';
var {
    Component,
    View,
    Text
} = React;

import {Actions} from 'react-native-router-flux';
import AppEventEmitter from '../services/AppEventEmitter';
var Icon = require('react-native-vector-icons/FontAwesome');

import {
    styles,
    NAV_TITLE_COLOR,
    NAV_HAMBURGER_SIZE,
} from '../styles/Styles';


export default class Back extends Component {
    onPress() {
        AppEventEmitter.emit('back.press');
    }

    render() {
        return (
            <View style={styles.back}>
                <Icon
                    name="angle-left"
                    size={NAV_HAMBURGER_SIZE}
                    color={NAV_TITLE_COLOR}
                    style={styles.hamburger}
                    onPress={this.onPress.bind(this)}
                />
                <Text
                    style={styles.backText}
                    onPress={this.onPress.bind(this)}>
                    Back
                </Text>
            </View>
        )
    }
}
