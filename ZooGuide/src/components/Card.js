'use strict';

import React from 'react-native';
var {
    Component,
    View,
    Text,
    Image,
    TouchableHighlight
} = React;

import {Actions} from 'react-native-router-flux';
import AppEventEmitter from '../services/AppEventEmitter';

import {
    styles,
    NAV_TITLE_COLOR,
    NAV_HAMBURGER_SIZE,
} from '../styles/Styles';


export default class Card extends Component {
    onPress() {
        AppEventEmitter.emit('card.press', {id: this.props.id });
    }

    render() {
        return (
            <TouchableHighlight onPress={this.onPress.bind(this)}>
                <View style={styles.card}>
                    <View style={styles.cardTop}>
                        <Image
                            style={styles.cardImage}
                            resizeMode="cover"
                            source={{ uri: this.props.uri}}
                        />
                    </View>

                    <View style={styles.cardFooter}>
                        <Text style={styles.cardFooterText}>{this.props.text}</Text>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }
}
