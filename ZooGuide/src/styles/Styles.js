'use strict';

import React, {
    StyleSheet,
    Dimensions
} from 'react-native';

const window = Dimensions.get('window');

// Colors
export var DARK_PRIMARY_COLOR = '#388E3C';
export var DEFAULT_PRIMARY_COLOR = '#4CAF50';
export var LIGHT_PRIMARY_COLOR = '#C8E6C9';
export var TEXT_PRIMARY_COLOR = '#FFFFFF';
export var ACCENT_COLOR = '#8BC34A';
export var PRIMARY_TEXT_COLOR = '#212121';
export var SECONDARY_TEXT_COLOR = '#727272';
export var DIVIDER_COLOR = '#B6B6B6';

export var NAV_TITLE_COLOR = 'white';
export var NAV_HAMBURGER_COLOR = 'white';
export var NAV_HAMBURGER_SIZE = 20;

export var HEART_COLOR_SELECTED = 'green';
export var HEART_COLOR = 'gray';
export var HEART_SIZE = 20;

export var MENU_ICON_SIZE = 30;

export var styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },

    paddedContent: {
        padding: 10
    },

    header: {
        color: PRIMARY_TEXT_COLOR,
        fontSize: 18,
        marginBottom: 5
    },

    text: {
        color: SECONDARY_TEXT_COLOR,
        fontSize: 12,
        marginBottom: 5
    },

    // Separator
    lineSeparator: {
        borderBottomColor: DEFAULT_PRIMARY_COLOR,
        borderBottomWidth: 1,
        marginBottom: 5
    },

    // Navigation Bar
    navigationBar: {
        backgroundColor: DEFAULT_PRIMARY_COLOR
    },

    // Hamburger
    hamburger: {
        marginLeft: 10
    },

    back: {
        flex: 1,
        flexDirection: 'row'
    },
    backText: {
        marginTop: 0,
        marginLeft: 4,
        color: TEXT_PRIMARY_COLOR,
        fontSize: 17,
        letterSpacing: 0.5
    },

    // Heart
    heart: {
        marginBottom: 5,
        width: HEART_SIZE,
        height: HEART_SIZE
    },

    // Cards
    card: {
        flex: 1,
        flexDirection: 'column'
    },
    cardTop: {
        flex: 0.8,
    },
    cardImage: {
        height: 101,
        width: 128,
        borderRadius: 3,
    },
    cardFooter: {
        flex: 0.2,
        padding: 5,
        backgroundColor: DARK_PRIMARY_COLOR,
        justifyContent: 'center',
    },
    cardFooterText: {
        textAlign: 'center',
        color: TEXT_PRIMARY_COLOR
    },

    // Menu
    rowSeparator: {
        flexDirection: 'row',
        height: 1,
        backgroundColor: DEFAULT_PRIMARY_COLOR
    },

    sectionSeparator: {
        flexDirection: 'row',
        height: 20,
        backgroundColor: DEFAULT_PRIMARY_COLOR
    },

    menuContainer: {
        flex: 1,
        backgroundColor: DEFAULT_PRIMARY_COLOR,
        width: window.width,
        height: window.height,
        paddingTop: 75
    },

    menuItemContainer: {
        backgroundColor: DEFAULT_PRIMARY_COLOR,
        paddingLeft: 0,
        padding: 5
    },

    menuItemIcon: {
        marginLeft: 12,
        marginRight: 24
    },

    menuItemText: {
        color: TEXT_PRIMARY_COLOR,
        fontSize: 25
    },

    menuItem: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderLeftColor: DEFAULT_PRIMARY_COLOR,
        borderLeftWidth: 3,
        padding: 5
    },

    menuItemSelected: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderLeftColor: ACCENT_COLOR,
        borderLeftWidth: 3,
        padding: 5
    },

    // Lists
    listView: {
        paddingTop: 0
    },
    rowView: {
        flex: 1,
        flexDirection: 'row',
        padding: 10
    },
    rowRightContainer: {
        flex: 1
    },
    rowTitleText: {
        color: "black",
        fontSize: 18,
        marginBottom: 4,
        textAlign: 'left'
    },
    rowSnippetText: {
        color: "darkgray",
        fontSize: 12,
        marginBottom: 8,
        textAlign: 'left',
        marginRight: 10,
        lineHeight: 13
    },
    rowThumbnailImage: {
        width: 70,
        height: 100,
        marginLeft: 5,
        marginRight: 10
    }
});