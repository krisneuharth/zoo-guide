'use strict';

import React, {
    Component,
    Text,
    View,
} from 'react-native';

import NavBar from '../components/NavBar';
import {styles} from '../styles/Styles';


export default class Map extends Component {
    render() {
        return (
            <View style={styles.container}>
                <NavBar title="Map"/>

                <Text style={styles.text}>
                    Map!
                </Text>
            </View>
        );
    }
}
