'use strict';

import React, {
    Component,
    Text,
    View,
    ListView,
    TouchableHighlight,
    Image
} from 'react-native';

import {Actions} from 'react-native-router-flux';
var Icon = require('react-native-vector-icons/FontAwesome');

import NavBar from '../components/NavBar';

import {
    styles,
    ACCENT_COLOR
} from '../styles/Styles';


var ANIMALS = require('../../data/Animals.js');


export default class Animals extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSource: new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2
            }),
            loaded: false
        }
    }

    componentDidMount() {
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(ANIMALS.animals),
            loaded: true
        });
    }

    gotoAnimal(row) {
        Actions.animal({
            animal: row
        });
    }

    renderRow(row) {
        return (
            <View>
                <TouchableHighlight
                    underlayColor={ACCENT_COLOR}
                    onPress={() => this.gotoAnimal(row)}>
                    <View style={styles.rowView}>
                        <View style={styles.rowRightContainer}>
                            <Text style={styles.rowTitleText}>{ row.name }</Text>
                            <Text style={styles.rowSnippetText}>{ row.snippet }</Text>
                        </View>
                    </View>
                </TouchableHighlight>

                <View style={styles.rowSeparator}/>
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <NavBar title={'Animals'} />

                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={this.renderRow.bind(this)}
                    style={styles.listView}
                />
            </View>
        );
    }
}
