'use strict';

import React, {
    Component,
    Text,
    View,
} from 'react-native';

import NavBar from '../components/NavBar';
import {styles} from '../styles/Styles';


export default class Settings extends Component {
    render() {
        return (
            <View style={styles.container}>
                <NavBar title="Settings"/>

                <Text style={styles.text}>
                    Settings!
                </Text>
            </View>
        );
    }
}
