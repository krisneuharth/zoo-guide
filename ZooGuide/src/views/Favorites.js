'use strict';

import React, {
    Component,
    Text,
    View,
} from 'react-native';

import NavBar from '../components/NavBar';
import {styles} from '../styles/Styles';

export default class Favorites extends Component {
    render() {
        return (
            <View style={styles.container}>
                <NavBar title={'Favorites'} />

                <Text style={styles.text}>
                    Favorites!
                </Text>
            </View>
        );
    }
}
