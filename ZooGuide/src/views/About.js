'use strict';

import React, {
    Component,
    Text,
    View,
} from 'react-native';

import NavBar from '../components/NavBar';
import {styles} from '../styles/Styles';

export default class About extends Component {
    render() {
        return (
            <View style={styles.container}>
                <NavBar title={'About'} />

                <Text style={styles.text}>
                    About!
                </Text>
            </View>
        );
    }
}
