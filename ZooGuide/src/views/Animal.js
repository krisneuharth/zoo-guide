'use strict';

import React, {
    Component,
    Text,
    View,
} from 'react-native';

import NavBar from '../components/NavBar';
import {styles} from '../styles/Styles';

export default class Animal extends Component {
    render() {
        return (
            <View style={styles.container}>
                <NavBar title={this.props.animal.name} backEnabled={true} />

                <Text style={styles.text}>
                    {this.props.animal.name}
                </Text>

                <Text style={styles.text}>
                    {this.props.animal.scientific_name}
                </Text>

                <Text style={styles.text}>
                    {this.props.animal.snippet}
                </Text>

                <Text style={styles.text}>
                    {this.props.animal.url}
                </Text>
            </View>
        );
    }
}
