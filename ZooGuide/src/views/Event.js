'use strict';

import React, {
    Component,
    Text,
    View,
} from 'react-native';

import NavBar from '../components/NavBar';
import {styles} from '../styles/Styles';


export default class Event extends Component {
    render() {
        return (
            <View style={styles.container}>
                <NavBar title={this.props.event.name} backEnabled={true} />

                <Text style={styles.text}>
                    Event!
                </Text>
            </View>
        );
    }
}
