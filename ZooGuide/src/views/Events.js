'use strict';

import React, {
    Component,
    Text,
    View,
} from 'react-native';

import NavBar from '../components/NavBar';
import {styles} from '../styles/Styles';

export default class Events extends Component {
    render() {
        return (
            <View style={styles.container}>
                <NavBar title={'Events'} />

                <Text style={styles.text}>
                    Events!
                </Text>
            </View>
        );
    }
}
