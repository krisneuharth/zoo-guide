'use strict';

import React, {
    Component,
    Text,
    View,
    Navigator,
    TouchableHighlight,
    ScrollView
} from 'react-native';

import Carousel from "react-native-carousel-control";
import {Actions} from 'react-native-router-flux';

import TitledCarousel from '../components/TitledCarousel';
import Heart from '../components/Heart';
import Separator from '../components/Separator';
import NavBar from '../components/NavBar';

import {styles} from '../styles/Styles';


export default class Home extends Component {
    render() {
        return (
            <View style={styles.container}>
                <NavBar title="Home"/>

                <ScrollView showsVerticalScrollIndicator={false}>
                    <TitledCarousel text="Popular Animals" animals={[1,2,3]}/>
                    <TitledCarousel text="Unpopular Animals" animals={[1,2]}/>
                    <TitledCarousel text="Mighty Mammals" animals={[1]}/>
                    <TitledCarousel text="Slithering Snakes" animals={[1,3]}/>
                    <TitledCarousel text="Australian Continent" animals={[1,2,3,2,1]}/>
                </ScrollView>

            </View>
        );
    }
}
