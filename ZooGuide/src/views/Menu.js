'use strict';

import React, {
    Component,
    Text,
    View,
    ScrollView,
    Image,
    TouchableHighlight
} from 'react-native';

var Icon = require('react-native-vector-icons/FontAwesome');
import {Actions} from 'react-native-router-flux';
import AppEventEmitter from '../services/AppEventEmitter';



import {
    styles,
    DARK_PRIMARY_COLOR,
    DEFAULT_PRIMARY_COLOR,
    TEXT_PRIMARY_COLOR,
    LIGHT_PRIMARY_COLOR,
    ACCENT_COLOR,
    MENU_ICON_SIZE
} from '../styles/Styles';

import Search from './Search';
import Home from './Home';
import Map from './Map';
import Animals from './Animals';
import Events from './Events';
import Favorites from './Favorites';
import Settings from './Settings';
import About from './About';

import MenuItem from '../components/MenuItem';


export default class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            'selected': 'Home'
        };
    }

    componentDidMount() {
		AppEventEmitter.addListener('menuItem.press', this.toggleMenuItemSelected.bind(this));
    }

    componentWillUnMount() {
    	AppEventEmitter.removeListener('menuItem.press');
    }

    toggleMenuItemSelected(args) {
        this.setState({
            selected: args.selected
        })
    }

    render() {
        return (
            <ScrollView scrollsToTop={false} style={styles.menuContainer}>
                <View style={styles.rowSeparator}/>

                <MenuItem name="Search" iconName="search" selected={this.state.selected == 'Search'}
                          gotoView={() => Actions.search({})}/>

                <View style={styles.rowSeparator}/>

                <MenuItem name="Home" iconName="home" selected={this.state.selected == 'Home'}
                          gotoView={() => Actions.home({})}/>

                <View style={styles.rowSeparator}/>

                <MenuItem name="Map" iconName="map-o" selected={this.state.selected == 'Map'}
                          gotoView={() => Actions.map({})}/>

                <View style={styles.rowSeparator}/>

                <MenuItem name="Animals" iconName="paw" selected={this.state.selected == 'Animals'}
                          gotoView={() => Actions.animals({})}/>

                <View style={styles.rowSeparator}/>

                <MenuItem name="Events" iconName="clock-o" selected={this.state.selected == 'Events'}
                          gotoView={() => Actions.events({})}/>

                <View style={styles.rowSeparator}/>

                <MenuItem name="Favorites" iconName="heart" selected={this.state.selected == "Favorites"}
                          gotoView={() => Actions.favorites({})}/>

                <View style={styles.rowSeparator}/>

                <View style={styles.sectionSeparator}/>

                <View style={styles.rowSeparator}/>

                <MenuItem name="Settings" iconName="cog" selected={this.state.selected == "Settings"}
                          gotoView={() => Actions.settings({})}/>

                <View style={styles.rowSeparator}/>

                <MenuItem name="About" iconName="info-circle" selected={this.state.selected == "About"}
                          gotoView={() => Actions.about({})}/>

                <View style={styles.rowSeparator}/>
            </ScrollView>
        );
    }
};